import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: './modules/home/home.module#HomeModule',
    data: { preload: false },
  },
  {
    path: 'quiz',
    loadChildren: './modules/quiz/quiz.module#QuizModule',
    data: { preload: false },
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/home'
  },
  { path: '**', redirectTo: '/home' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
