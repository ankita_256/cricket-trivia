import { Component, Input, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

export const errorMessages = {
  REQUIRED: 'This field is required.'
}

@Component({
  selector: 'app-control-messages',
  templateUrl: './control-messages.component.html',
  styleUrls: ['./control-messages.component.scss']
})
export class ControlMessagesComponent implements OnInit {

  @Input() control: FormControl;

  constructor() { }

  ngOnInit() {

  }

  config(key, validatorValue) {
    const _config = {
      'required': errorMessages.REQUIRED
    };
    return _config[key];
  }

  get errorMessage() {
    if (this.control.dirty && this.control.touched) {
      for (const propertyName in this.control.errors) {
        if (this.control.errors.hasOwnProperty(propertyName)) {
          return this.control.errors.label || this.config(propertyName, this.control.errors);
        }
      }
    }
    return '';
  }
}
