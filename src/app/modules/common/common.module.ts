import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ControlMessagesComponent } from './components/control-messages/control-messages.component';

@NgModule({
  declarations: [
    ControlMessagesComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ControlMessagesComponent
  ],
  providers: []
})
export class OurCommonModule { }
