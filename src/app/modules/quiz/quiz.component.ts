import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../../services/questions.service';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.scss']
})
export class QuizComponent implements OnInit {

  public quest: any;
  public form: FormGroup;
  public data: any;
  public submitted: boolean = false;
  public correct_answers: number = 0;

  public chartOptions = {
    scales: {
      yAxes: [{
        ticks: {
          stepSize: 1,
          beginAtZero: true
        }
      }]
    },
    legend: {
      display: false
    }
  };

  constructor(private questionsService: QuestionsService, public fb: FormBuilder, private router: Router) {
  }

  ngOnInit() {
    this.questionsService.getJSON().subscribe(response => {
      this.quest = response.questions;
      console.log(this.quest);
      this.initForm();
    });
  }

  /**
   * initialize form
   */
  initForm() {
    let group: any = {};
    this.quest.forEach(element => {
      group[element['control']] = new FormControl('', [Validators.required]);
    });
    this.form = this.fb.group(group);

  }

  /**
   * reset form
   */
  resetForm() {
    this.form.reset();
  }

  /**
   * submit form
   */
  submitForm() {
    if (!this.form.valid) {
      return this.questionsService.validateAllFormFields(this.form);
    }
    this.submitted = true;
    let correct = 0,incorrect = 0;
    this.quest.forEach((element, index) => {
      if(element['correct_answer'][0] === this.form.value['question_' + index]['value']) {
        correct++;
        element['correct'] = true;
      }
      else
      incorrect++;
    });
    this.form.disable();
    this.correct_answers = correct;
    this.showGraph(incorrect, correct);
  }

  /**
   * display result
   */
  showGraph(incorrect,correct) {
    this.data = {
      labels: ['Incorrect', 'Correct'],
      datasets: [
        {
          label: 'My First dataset',
          borderColor: '#1E88E5',
          data: [incorrect,correct],
          color: ["red", "green"]
        }
      ]
    };
    window.scroll(0,0);
  }

  reload() {
    this.resetForm();
    this.form.enable();
    this.submitted = false;
    this.data = '';
  }

}
