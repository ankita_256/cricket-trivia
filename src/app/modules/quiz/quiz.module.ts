import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuizComponent } from './quiz.component';
import { Routes, RouterModule } from '@angular/router';
import { DropdownModule } from 'primeng/dropdown';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OurCommonModule } from 'src/app/modules/common/common.module';
import { ChartModule } from 'primeng/chart'

const quizRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: QuizComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(quizRoutes),
    DropdownModule,
    FormsModule,
    ReactiveFormsModule,
    OurCommonModule,
    ChartModule
  ],
  declarations: [
    QuizComponent
  ],
  providers: [  ]
})
export class QuizModule { }
