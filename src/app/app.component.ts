import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [
    './app.component.scss',
     '../../node_modules/primeng/resources/components/dropdown/dropdown.css',
     '../../node_modules/primeng/resources/components/common/common.css',
     '../../node_modules/primeicons/primeicons.css'
    ],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'cricket-trivia';
}
