import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FormGroup, FormControl, FormArray } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class QuestionsService {

  constructor(private http: HttpClient) {
    this.getJSON().subscribe(data => {
      //console.log(data);
    });
  }

  public getJSON(): Observable<any> {
    return this.http.get("../../assets/questions.json");
  }

  /**
   * to fire all errors before submission
   * @param form FormGroup to be validated.
   */
  public validateAllFormFields(form: FormGroup) {
    const keys = Object.keys(form.controls);
    keys.forEach((field: any) => {
      const control = form.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
        control.markAsDirty({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      } else if (control instanceof FormArray) {
        (<FormArray>control).controls.forEach((element: FormGroup) => {
          this.validateAllFormFields(element);
        });
      }
    });
  }
}
